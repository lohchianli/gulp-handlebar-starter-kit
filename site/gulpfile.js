const stripDebug = require('gulp-strip-debug');

// generated on 2017-03-27 using generator-webapp 2.1.0
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const wiredep = require('wiredep').stream;
const argv = require('minimist')(process.argv.slice(2));
const processhtml = require('gulp-processhtml');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');

var Dir = 'site/';
var envDir = '../_env/';
var distDir = 'dist/';

var jsConfigPath = '';
var scssConfigPath = '';
var templatesConfig = {};


console.log('argv', argv);

// arguments for environment

if (['localhost', 'staging', 'production'].indexOf(argv.env) > -1) {
  jsConfigPath = '../_env/' + argv.env + '/scripts/';
  scssConfigPath = '../_env/' + argv.env + '/styles/';
  templatesConfig.data = '../_env/' + argv.env + '/templates/';
}
else {
  jsConfigPath = '../_env/scripts/localhost/data.js';
  scssConfigPath = '../_env/localhost/styles/';
  templatesConfig.data = '../_env/localhost/templates/';
}

// templates to build - currently only one, no other variations required
templatesConfig.src = './app/source/templates/default/*.hbs';
templatesConfig.dest = 'default/';


console.log('jsConfigPath',jsConfigPath);
console.log('scssConfigPath',scssConfigPath);


gulp.task('handlebars', function() {

  var templateData = require('./'+templatesConfig.data+'data.json')
    , options = {
      ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false
      // partials : {
      //     footer : '<footer>the end</footer>'
      // },
      batch: ['app/source/partials']
      , helpers: {
        capitals: function(str) {
          return str.toUpperCase();
        }
      }
    }

  return gulp.src(templatesConfig.src)
    .pipe(handlebars(templateData, options))
    // .pipe(gulp.dest('app'))
    .pipe(rename({extname: ".html"}))
    .pipe(gulp.dest('.tmp/' + templatesConfig.dest));
});


gulp.task('styles', () => {
  return gulp.src('app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.', scssConfigPath]
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/' + templatesConfig.dest + 'styles'))
    .pipe(reload({stream: true}));
});

gulp.task('styles:dist', () => {
  return gulp.src('app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sass.sync({
      outputStyle: 'compressed',
      precision: 10,
      includePaths: ['.', scssConfigPath]
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe(gulp.dest('.tmp/' + templatesConfig.dest + 'styles'))
    .pipe(reload({stream: true}));
});


gulp.task('scripts', () => {
  return gulp.src([jsConfigPath+'data.js', 'app/scripts/*.js'])
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.concat('main.js'))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/'+ templatesConfig.dest +'scripts'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
appendFile}

gulp.task('lint', () => {
  // return lint('app/scripts/**/*.js', {
  //   fix: true
  // })
  //   .pipe(gulp.dest('app/scripts'));
});

gulp.task('html', ['handlebars', 'styles', 'scripts'], () => {
  return gulp.src('.tmp/'+ templatesConfig.dest +'/*.html')
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe(gulp.dest('.tmp/' + templatesConfig.dest));
});

gulp.task('html:dist', ['handlebars', 'styles:dist', 'scripts'], () => {
  return gulp.src('.tmp/'+ templatesConfig.dest +'/*.html')
    .pipe(processhtml({}))
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.js', $.stripDebug()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe(gulp.dest('.tmp/' + templatesConfig.dest));
});

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    // .pipe($.cache($.imagemin({
    //   progressive: true,
    //   interlaced: true,
    //   // don't remove IDs from SVGs, they are often used
    //   // as hooks for embedding and styling
    //   svgoPlugins: [{cleanupIDs: false}]
    // })))
    .pipe(gulp.dest('.tmp/'+ templatesConfig.dest +'images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{otf,eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('app/fonts/**/*'))
    .pipe(gulp.dest('.tmp/'+ templatesConfig.dest +'fonts'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*.*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('.tmp/' + templatesConfig.dest));
});

gulp.task('clean', del.bind(null, ['.tmp/', distDir ]));


// create a task that ensures the `handlebars` task is complete before
// reloading browsers
gulp.task('handlebar-watch', ['handlebars'], function(done) {
  browserSync.reload();
  done();
});

gulp.task('serve', ['handlebars', 'images', 'styles', 'scripts', 'fonts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    
    server: {
      baseDir: ['.tmp', '.tmp/' + templatesConfig.dest],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch([
    'app/source/templates/**/*',
    'app/source/partials/*',
    'app/images/**/*'
  ], ['handlebar-watch']);


  gulp.watch('app/**/*', ['handlebars', 'styles', 'scripts'])
  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('app/fonts/**/*', ['fonts']);
  gulp.watch('bower.json', ['wiredep', 'fonts']);
});

gulp.task('serve:dist', ['build'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist/' + templatesConfig.dest]
    }
  });
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('app/styles/*.scss')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    // .pipe(gulp.dest('app'));
    .pipe(gulp.dest(templatesConfig.dest));
});

gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
  return gulp.src(['.tmp/'+templatesConfig.dest+'**/*'])
    .pipe(gulp.dest('dist/' + templatesConfig.dest));
});

gulp.task('build:dist', ['lint', 'html:dist', 'images', 'fonts', 'extras'], () => {
  return gulp.src(
      ['.tmp/'+templatesConfig.dest+'**/*'])
    .pipe($.size({title: 'build', gzip: true}))
    .pipe(gulp.dest(distDir + templatesConfig.dest));
});

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
